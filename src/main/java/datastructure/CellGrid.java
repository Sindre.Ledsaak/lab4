package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int columns;
    CellState [][] grid;
    


    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.columns = columns;
        grid = new CellState [rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int x = 0; x < columns; x++) {
                grid [i][x] = initialState;
            }
            
        }
                
            }

        

    @Override
    public int numRows() {
        
        return this.rows;
    }

    @Override
    public int numColumns() {
        
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        
        this.grid [row] [column] = element;
        
        
    }

    @Override
    public CellState get(int row, int column) {
        
        return grid [row] [column];
    }

    @Override
    public IGrid copy() {
        IGrid newgrid = new CellGrid(this.rows, this.columns, CellState.DEAD);
        for (int x = 0; x < this.rows ; x++) {
            for (int i = 0; i < this.columns; i++) {
                newgrid.set(x,i, this.grid[x][i]);
                
            }
            
        }
        return newgrid;
    }
    
}