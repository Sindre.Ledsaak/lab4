package cellular;
import java.util.Random;

import cellular.BriansBrain;
import cellular.CellAutomaton;
import cellular.GameOfLife;
import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}
    public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		
		return currentGeneration.get(row, col) ;
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		
		for (int x = 0; x < numberOfRows(); x++) {
			for (int y = 0; y < numberOfColumns(); y++) {
				nextGeneration.set(x, y, getNextCell(x, y));
				
			}
			
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState currentstate = getCellState(row, col);
		int n = countNeighbors(row, col,CellState.ALIVE);
		if (currentstate == CellState.ALIVE){
            return CellState.DYING;}
        else if (currentstate == CellState.DYING){
            return CellState.DEAD;
        }
		else if (currentstate == CellState.DEAD & n==2){
			return CellState.ALIVE;
		}
		else {return CellState.DEAD; }
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int stateCount = 0;
		for (int r = Math.max(0, row-1); r < Math.min(currentGeneration.numRows(), row+2); r++) {
			for (int c = Math.max(0, col-1); c < Math.min(currentGeneration.numColumns(), col+2); c++) {
				if (!((r==row)&&(c==col))) {
					
					if (getCellState(r, c).equals(state)) {
						stateCount++;
					}
				} 
			}
		}
		return stateCount;
	}

    public IGrid getGrid() {
		return currentGeneration;
	}

  
}